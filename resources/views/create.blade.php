@extends('layouts/master')
@section('content')
<div class="ml-4 mt-4 mr-4">
    <div>
            <form action="/buku" method="POST">
                @csrf
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" class="form-control" name="judul" placeholder="Masukkan judul Anda">
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Pengarang</label>
                    <input type="text" class="form-control" name="pengarang" placeholder="Masukkan Nama Pengarang">
                    @error('pengarang')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
                    @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" class="form-control" name="tahun" placeholder="Masukkan Tahun Terbit">
                    @error('tahun')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
    </div>
      </div>
</div>
@endsection