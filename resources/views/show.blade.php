@extends('layouts.master')

@section('show')
    <h2>Show buku {{$buku->id}}</h2>
    <h4>{{$buku->judul}}</h4>
    <p>{{$buku->pengarang}}</p>
    <p>{{$buku->deskripsi}}</p>
    <p>{{$buku->tahun}}</p>
@endsection