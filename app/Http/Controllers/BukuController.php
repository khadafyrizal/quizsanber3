<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class BukuController extends Controller
{
    public function create(){
        return view('create');
    }
    public function store(Request $request){
        $request->validate([
            'judul' => 'required|max:50',
            'Pengarang' => 'required|max:40',
            'deskripsi' => 'required',
            'tahun' => 'required|max:4',
        ]);
        DB::table('buku')->insert(
            [
                'judul' => $request['judul'],
                'pengarang' => $request['pengarang'],
                'deskripsi' => $request['deskripsi'],
                'tahun' => $request['tahun'],
            ]
        );
    }  
        public function index(){
            $buku = DB::table('buku')->get();
            return view('index', compact('buku'));
        }
        public function show($id){
            $buku = DB::table('buku')->where('id',$id)->first();
            return view('show', compact('buku'));
        }
        public function edit($id)
    {
        $buku = DB::table('buku')->where('id', $id)->first();
        return view('edit', compact('buku'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|max:50',
            'Pengarang' => 'required|max:40',
            'deskripsi' => 'required',
            'tahun' => 'required|max:4',
        ]);

        $query = DB::table('buku')
            ->where('id', $id)
            ->update([
                'judul' => $request['judul'],
                'pengarang' => $request['pengarang'],
                'deskripsi' => $request['deskripsi'],
                'tahun' => $request['tahun'],
            ]);
        return redirect('/buku');
    }
    public function destroy($id)
    {
        $query = DB::table('buku')->where('id', $id)->delete();
        return redirect('/buku');
    }
}
